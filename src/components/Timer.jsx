import React from 'react'

class Timer extends React.Component {
    timer = undefined
    constructor(props) {
        super (props)
        this.state = {
            secondNumber: 0
        }
        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
    }

    startTimer() {
        this.timer = setInterval(()=>{
            this.setState({
                secondNumber: this.state.secondNumber + 1
            })
        }, 1000)
    }

    stopTimer() {
        clearInterval(this.timer)
    }

    render() {
        return (<div>
            <p>Timer: {this.state.secondNumber} detik</p>
            <button className="btn btn-primary mr-3" onClick={this.startTimer}>Start</button>
            <button className="btn btn-danger" onClick={this.stopTimer}>Stop</button>
        </div>)
    }
}

export default Timer