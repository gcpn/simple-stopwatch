import React from 'react'

class Warna extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            warna: 'Biru'
        }
    }

    componentDidMount() {
        setTimeout(()=>{
            this.setState(
                {warna: 'Merah'}
            )
        }, 2000)
    }

    render() {
        return (
        <h1>Warna yang saya set via state = {this.state.warna}</h1>
        )
    }
}

export default Warna