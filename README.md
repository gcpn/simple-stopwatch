# Tugas 1 - Membuat Timer menggunakan ReactJS

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Membuat timer (dalam hal ini stopwatch) sederhana dengan ReactJS. Tombol ‘Start’ akan memulai penghitungan waktu dan tombol ‘Stop’ akan mengentikan penghitungan waktu. Angka detik akan sama dengan nol atau angka ketika tombol Stop diklik. Fungsi utama disematkan di event klik kedua button.
